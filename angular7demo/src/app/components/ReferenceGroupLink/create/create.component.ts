import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ReferenceGroupLinkService } from '../../../services/ReferenceGroupLink.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../ReferenceGroupLink/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateReferenceGroupLinkComponent extends SubBaseComponent implements OnInit {

  title = 'Add ReferenceGroupLink';
  referenceGroupLinkForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private referenceGroupLinkservice: ReferenceGroupLinkService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.referenceGroupLinkForm = this.fb.group({
      dateLinkCreated: ['', Validators.required],
      name: ['', Validators.required],
      ReferrerGroup: ['', ],
      LinkProvider: ['', ]
   });
  }
  addReferenceGroupLink(dateLinkCreated, name, ReferrerGroup, LinkProvider) {
      this.referenceGroupLinkservice.addReferenceGroupLink(dateLinkCreated, name, ReferrerGroup, LinkProvider)
      	.then(success => this.router.navigate(['/indexReferenceGroupLink']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
