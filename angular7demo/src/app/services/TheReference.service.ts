import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {TheReference} from '../models/TheReference';
import {QuestionGroupService} from '../services/QuestionGroup.service';
import {UserService} from '../services/User.service';
import {ReferrerService} from '../services/Referrer.service';
import {AnswerService} from '../services/Answer.service';
import {QuestionService} from '../services/Question.service';


@Injectable()
export class TheReferenceService {

	//********************************************************************
	// general holder 
	//********************************************************************
	theReference : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {}
 	
	//********************************************************************
	// add a TheReference 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addTheReference(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered) : Promise<any> {
    	const uri = 'http://localhost:4200/TheReference/add';
    	const obj = {
      		dateLastUpdated: dateLastUpdated,
      		active: active,
      		dateTimeLastViewedExternally: dateTimeLastViewedExternally,
      		makeViewableToUser: makeViewableToUser,
      		dateLastSent: dateLastSent,
      		numDaysToExpire: numDaysToExpire,
      		QuestionGroup: QuestionGroup != null && QuestionGroup.length > 0 ? QuestionGroup : null,
      		Status: Status,
      		Type: Type,
      		User: User != null && User.length > 0 ? User : null,
      		Referrer: Referrer != null && Referrer.length > 0 ? Referrer : null,
      		Answers: Answers != null && Answers.length > 0 ? Answers : null,
			LastQuestionAnswered: LastQuestionAnswered != null && LastQuestionAnswered.length > 0 ? LastQuestionAnswered : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all TheReference 
	// returns the results untouched as JSON representation of an
	// array of TheReference models
	// delegates via URI to an ORM handler
	//********************************************************************
	getTheReferences() {
    	const uri = 'http://localhost:4200/TheReference';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a TheReference 
	// returns the results untouched as a JSON representation of a
	// TheReference model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editTheReference(id) {
    	const uri = 'http://localhost:4200/TheReference/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a TheReference 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateTheReference(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered, id)  : Promise<any>  {
    	const uri = 'http://localhost:4200/TheReference/update/' + id;
    	const obj = {
      		dateLastUpdated: dateLastUpdated,
      		active: active,
      		dateTimeLastViewedExternally: dateTimeLastViewedExternally,
      		makeViewableToUser: makeViewableToUser,
      		dateLastSent: dateLastSent,
      		numDaysToExpire: numDaysToExpire,
      		QuestionGroup: QuestionGroup != null && QuestionGroup.length > 0 ? QuestionGroup : null,
      		Status: Status,
      		Type: Type,
      		User: User != null && User.length > 0 ? User : null,
      		Referrer: Referrer != null && Referrer.length > 0 ? Referrer : null,
      		Answers: Answers != null && Answers.length > 0 ? Answers : null,
			LastQuestionAnswered: LastQuestionAnswered != null && LastQuestionAnswered.length > 0 ? LastQuestionAnswered : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a TheReference 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteTheReference(id)  : Promise<any> {
    	const uri = 'http://localhost:4000/TheReference/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a QuestionGroup on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignQuestionGroup( theReferenceId, questionGroupId ): Promise<any> {

		// get the TheReference from storage
		this.loadHelper( theReferenceId );
		
		// get the QuestionGroup from storage
		var questionGroup 	= new QuestionGroupService(this.http).editQuestionGroup(questionGroupId);
		
		// assign the QuestionGroup		
		this.theReference.questionGroup = questionGroup;
      		
		// save the TheReference
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a QuestionGroup on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignQuestionGroup( theReferenceId ): Promise<any> {

		// get the TheReference from storage
        this.loadHelper( theReferenceId );
		
		// assign QuestionGroup to null		
		this.theReference.questionGroup = null;
      		
		// save the TheReference
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a User on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignUser( theReferenceId, userId ): Promise<any> {

		// get the TheReference from storage
		this.loadHelper( theReferenceId );
		
		// get the User from storage
		var user 	= new UserService(this.http).editUser(userId);
		
		// assign the User		
		this.theReference.user = user;
      		
		// save the TheReference
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a User on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignUser( theReferenceId ): Promise<any> {

		// get the TheReference from storage
        this.loadHelper( theReferenceId );
		
		// assign User to null		
		this.theReference.user = null;
      		
		// save the TheReference
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a Referrer on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignReferrer( theReferenceId, referrerId ): Promise<any> {

		// get the TheReference from storage
		this.loadHelper( theReferenceId );
		
		// get the Referrer from storage
		var referrer 	= new ReferrerService(this.http).editReferrer(referrerId);
		
		// assign the Referrer		
		this.theReference.referrer = referrer;
      		
		// save the TheReference
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Referrer on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignReferrer( theReferenceId ): Promise<any> {

		// get the TheReference from storage
        this.loadHelper( theReferenceId );
		
		// assign Referrer to null		
		this.theReference.referrer = null;
      		
		// save the TheReference
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a LastQuestionAnswered on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignLastQuestionAnswered( theReferenceId, lastQuestionAnsweredId ): Promise<any> {

		// get the TheReference from storage
		this.loadHelper( theReferenceId );
		
		// get the Question from storage
		var question 	= new QuestionService(this.http).editQuestion(lastQuestionAnsweredId);
		
		// assign the LastQuestionAnswered		
		this.theReference.lastQuestionAnswered = question;
      		
		// save the TheReference
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a LastQuestionAnswered on a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignLastQuestionAnswered( theReferenceId ): Promise<any> {

		// get the TheReference from storage
        this.loadHelper( theReferenceId );
		
		// assign LastQuestionAnswered to null		
		this.theReference.lastQuestionAnswered = null;
      		
		// save the TheReference
		return this.saveHelper();
	}
	

	//********************************************************************
	// adds one or more answersIds as a Answers 
	// to a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addAnswers( theReferenceId, answersIds ): Promise<any> {

		// get the TheReference
		this.loadHelper( theReferenceId );
				
		// split on a comma with no spaces
		var idList = answersIds.split(',')

		// iterate over array of answers ids
		idList.forEach(function (id) {
			// read the Answer		
			var answer = new AnswerService(this.http).editAnswer(id);	
			// add the Answer if not already assigned
			if ( this.theReference.answers.indexOf(answer) == -1 )
				this.theReference.answers.push(answer);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more answersIds as a Answers 
	// from a TheReference
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeAnswers( theReferenceId, answersIds ): Promise<any> {
		
		// get the TheReference
		this.loadHelper( theReferenceId );

				
		// split on a comma with no spaces
		var idList 					= answersIds.split(',');
		var answers 	= this.theReference.answers;
		
		if ( answers != null && answersIds != null ) {
		
			// iterate over array of answers ids
			answers.forEach(function (obj) {				
				if ( answersIds.indexOf(obj._id) > -1 ) {
					 // remove the Answer
					this.theReference.answers.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a TheReference
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = 'http://localhost:4200/TheReference/update/' + this.theReference._id;		
		
    	return this
      			.http
      			.post(uri, this.theReference)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a TheReference
	//********************************************************************	
	loadHelper( id ) {
		this.editTheReference(id)
        		.subscribe(res => {
        			this.theReference = res;
      			});
	}
}