import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {ReferenceEngine} from '../models/ReferenceEngine';
import {QuestionGroupService} from '../services/QuestionGroup.service';


@Injectable()
export class ReferenceEngineService {

	//********************************************************************
	// general holder 
	//********************************************************************
	referenceEngine : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {}
 	
	//********************************************************************
	// add a ReferenceEngine 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addReferenceEngine(name, active, MainQuestionGroup, Purpose) : Promise<any> {
    	const uri = 'http://localhost:4200/ReferenceEngine/add';
    	const obj = {
      		name: name,
      		active: active,
      		MainQuestionGroup: MainQuestionGroup != null && MainQuestionGroup.length > 0 ? MainQuestionGroup : null,
			Purpose: Purpose
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all ReferenceEngine 
	// returns the results untouched as JSON representation of an
	// array of ReferenceEngine models
	// delegates via URI to an ORM handler
	//********************************************************************
	getReferenceEngines() {
    	const uri = 'http://localhost:4200/ReferenceEngine';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a ReferenceEngine 
	// returns the results untouched as a JSON representation of a
	// ReferenceEngine model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editReferenceEngine(id) {
    	const uri = 'http://localhost:4200/ReferenceEngine/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a ReferenceEngine 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateReferenceEngine(name, active, MainQuestionGroup, Purpose, id)  : Promise<any>  {
    	const uri = 'http://localhost:4200/ReferenceEngine/update/' + id;
    	const obj = {
      		name: name,
      		active: active,
      		MainQuestionGroup: MainQuestionGroup != null && MainQuestionGroup.length > 0 ? MainQuestionGroup : null,
			Purpose: Purpose
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a ReferenceEngine 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteReferenceEngine(id)  : Promise<any> {
    	const uri = 'http://localhost:4000/ReferenceEngine/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a MainQuestionGroup on a ReferenceEngine
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignMainQuestionGroup( referenceEngineId, mainQuestionGroupId ): Promise<any> {

		// get the ReferenceEngine from storage
		this.loadHelper( referenceEngineId );
		
		// get the QuestionGroup from storage
		var questionGroup 	= new QuestionGroupService(this.http).editQuestionGroup(mainQuestionGroupId);
		
		// assign the MainQuestionGroup		
		this.referenceEngine.mainQuestionGroup = questionGroup;
      		
		// save the ReferenceEngine
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a MainQuestionGroup on a ReferenceEngine
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignMainQuestionGroup( referenceEngineId ): Promise<any> {

		// get the ReferenceEngine from storage
        this.loadHelper( referenceEngineId );
		
		// assign MainQuestionGroup to null		
		this.referenceEngine.mainQuestionGroup = null;
      		
		// save the ReferenceEngine
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a ReferenceEngine
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = 'http://localhost:4200/ReferenceEngine/update/' + this.referenceEngine._id;		
		
    	return this
      			.http
      			.post(uri, this.referenceEngine)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a ReferenceEngine
	//********************************************************************	
	loadHelper( id ) {
		this.editReferenceEngine(id)
        		.subscribe(res => {
        			this.referenceEngine = res;
      			});
	}
}