import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AnswerService } from '../../../services/Answer.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Answer/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateAnswerComponent extends SubBaseComponent implements OnInit {

  title = 'Add Answer';
  answerForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private answerservice: AnswerService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.answerForm = this.fb.group({
      Question: ['', ],
      Response: ['', ]
   });
  }
  addAnswer(Question, Response) {
      this.answerservice.addAnswer(Question, Response)
      	.then(success => this.router.navigate(['/indexAnswer']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
