import { TheResponseService } from '../../../services/TheResponse.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TheResponse } from '../../../models/TheResponse';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexTheResponseComponent implements OnInit {

  theResponses: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: TheResponseService) {}

  ngOnInit() {
    this.getTheResponses();
  }

  getTheResponses() {
    this.service.getTheResponses().subscribe(res => {
      this.theResponses = res;
    });
  }

  deleteTheResponse(id) {
    this.service.deleteTheResponse(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexTheResponse']));
			});  }
}
