// referenceGroupLinkRoutes.js

var express = require('express');
var app = express();
var referenceGroupLinkRoutes = express.Router();

// Require Item model in our routes module
var ReferenceGroupLink = require('../models/ReferenceGroupLink');

// Defined store route
referenceGroupLinkRoutes.route('/add').post(function (req, res) {
	var referenceGroupLink = new ReferenceGroupLink(req.body);
	referenceGroupLink.save()
    .then(item => {
    	res.status(200).json({'referenceGroupLink': 'ReferenceGroupLink added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
referenceGroupLinkRoutes.route('/').get(function (req, res) {
	ReferenceGroupLink.find(function (err, referenceGroupLinks){
		if(err){
			console.log(err);
		}
		else {
			res.json(referenceGroupLinks);
		}
	});
});

// Defined edit route
referenceGroupLinkRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	ReferenceGroupLink.findById(id, function (err, referenceGroupLink){
		res.json(referenceGroupLink);
	});
});

//  Defined update route
referenceGroupLinkRoutes.route('/update/:id').post(function (req, res) {
	ReferenceGroupLink.findById(req.params.id, function(err, referenceGroupLink) {
		if (!referenceGroupLink)
			return next(new Error('Could not load a ReferenceGroupLink Document using id ' + req.params.id));
		else {
            referenceGroupLink.dateLinkCreated = req.body.dateLinkCreated;
            referenceGroupLink.name = req.body.name;
            referenceGroupLink.ReferrerGroup = req.body.ReferrerGroup;
            referenceGroupLink.LinkProvider = req.body.LinkProvider;

			referenceGroupLink.save().then(referenceGroupLink => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
referenceGroupLinkRoutes.route('/delete/:id').get(function (req, res) {
   ReferenceGroupLink.findOneAndDelete({_id: req.params.id}, function(err, referenceGroupLink){
        if(err) res.json(err);
        else res.json('Successfully removed ' + ReferenceGroupLink + ' using id ' + req.params.id );
    });
});

module.exports = referenceGroupLinkRoutes;