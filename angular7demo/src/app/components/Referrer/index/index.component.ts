import { ReferrerService } from '../../../services/Referrer.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Referrer } from '../../../models/Referrer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexReferrerComponent implements OnInit {

  referrers: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferrerService) {}

  ngOnInit() {
    this.getReferrers();
  }

  getReferrers() {
    this.service.getReferrers().subscribe(res => {
      this.referrers = res;
    });
  }

  deleteReferrer(id) {
    this.service.deleteReferrer(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexReferrer']));
			});  }
}
