import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TheResponseService } from '../../../services/TheResponse.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../TheResponse/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateTheResponseComponent extends SubBaseComponent implements OnInit {

  title = 'Add TheResponse';
  theResponseForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private theResponseservice: TheResponseService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.theResponseForm = this.fb.group({
      responseText: ['', Validators.required],
      gotoQuestionId: ['', Validators.required]
   });
  }
  addTheResponse(responseText, gotoQuestionId) {
      this.theResponseservice.addTheResponse(responseText, gotoQuestionId)
      	.then(success => this.router.navigate(['/indexTheResponse']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
