import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TheReferenceService } from '../../../services/TheReference.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../TheReference/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditTheReferenceComponent extends SubBaseComponent implements OnInit {

  theReference: any;
  theReferenceForm: FormGroup;
  title = 'Edit TheReference';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: TheReferenceService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.theReferenceForm = this.fb.group({
      dateLastUpdated: ['', Validators.required],
      active: ['', Validators.required],
      dateTimeLastViewedExternally: ['', Validators.required],
      makeViewableToUser: ['', Validators.required],
      dateLastSent: ['', Validators.required],
      numDaysToExpire: ['', Validators.required],
      QuestionGroup: ['', ],
      Status: ['', ],
      Type: ['', ],
      User: ['', ],
      Referrer: ['', ],
      Answers: ['', ],
      LastQuestionAnswered: ['', ]
   });
  }
  updateTheReference(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered) {
    this.route.params.subscribe(params => {
    	this.service.updateTheReference(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered, params['id'])
      		.then(success => this.router.navigate(['/indexTheReference']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.theReference = this.service.editTheReference(params['id']).subscribe(res => {
        this.theReference = res;
      });
    });
    
    super.ngOnInit();
  }
}
