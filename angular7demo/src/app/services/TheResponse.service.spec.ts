import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { TheResponseService } from './TheResponse.service';

describe('TheResponseService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [TheResponseService] });
	});

  it('should be created', () => {
    const service: TheResponseService = TestBed.get(TheResponseService);
    expect(service).toBeTruthy();
  });
});
