// routerConfig.ts

import { Routes } from '@angular/router';
import { CreateUserComponent } from './components/User/create/create.component';
import { EditUserComponent } from './components/User/edit/edit.component';
import { IndexUserComponent } from './components/User/index/index.component';
import { CreateReferrerComponent } from './components/Referrer/create/create.component';
import { EditReferrerComponent } from './components/Referrer/edit/edit.component';
import { IndexReferrerComponent } from './components/Referrer/index/index.component';
import { CreateTheReferenceComponent } from './components/TheReference/create/create.component';
import { EditTheReferenceComponent } from './components/TheReference/edit/edit.component';
import { IndexTheReferenceComponent } from './components/TheReference/index/index.component';
import { CreateReferrerGroupComponent } from './components/ReferrerGroup/create/create.component';
import { EditReferrerGroupComponent } from './components/ReferrerGroup/edit/edit.component';
import { IndexReferrerGroupComponent } from './components/ReferrerGroup/index/index.component';
import { CreateReferenceEngineComponent } from './components/ReferenceEngine/create/create.component';
import { EditReferenceEngineComponent } from './components/ReferenceEngine/edit/edit.component';
import { IndexReferenceEngineComponent } from './components/ReferenceEngine/index/index.component';
import { CreateQuestionComponent } from './components/Question/create/create.component';
import { EditQuestionComponent } from './components/Question/edit/edit.component';
import { IndexQuestionComponent } from './components/Question/index/index.component';
import { CreateTheResponseComponent } from './components/TheResponse/create/create.component';
import { EditTheResponseComponent } from './components/TheResponse/edit/edit.component';
import { IndexTheResponseComponent } from './components/TheResponse/index/index.component';
import { CreateQuestionGroupComponent } from './components/QuestionGroup/create/create.component';
import { EditQuestionGroupComponent } from './components/QuestionGroup/edit/edit.component';
import { IndexQuestionGroupComponent } from './components/QuestionGroup/index/index.component';
import { CreateAdminComponent } from './components/Admin/create/create.component';
import { EditAdminComponent } from './components/Admin/edit/edit.component';
import { IndexAdminComponent } from './components/Admin/index/index.component';
import { CreateActivityComponent } from './components/Activity/create/create.component';
import { EditActivityComponent } from './components/Activity/edit/edit.component';
import { IndexActivityComponent } from './components/Activity/index/index.component';
import { CreateCommentComponent } from './components/Comment/create/create.component';
import { EditCommentComponent } from './components/Comment/edit/edit.component';
import { IndexCommentComponent } from './components/Comment/index/index.component';
import { CreateAnswerComponent } from './components/Answer/create/create.component';
import { EditAnswerComponent } from './components/Answer/edit/edit.component';
import { IndexAnswerComponent } from './components/Answer/index/index.component';
import { CreateReferenceGroupLinkComponent } from './components/ReferenceGroupLink/create/create.component';
import { EditReferenceGroupLinkComponent } from './components/ReferenceGroupLink/edit/edit.component';
import { IndexReferenceGroupLinkComponent } from './components/ReferenceGroupLink/index/index.component';

export const UserRoutes: Routes = [
  { path: 'createUser',
    component: CreateUserComponent
  },
  {
    path: 'editUser/:id',
    component: EditUserComponent
  },
  { path: 'indexUser',
    component: IndexUserComponent
  }
];
export const ReferrerRoutes: Routes = [
  { path: 'createReferrer',
    component: CreateReferrerComponent
  },
  {
    path: 'editReferrer/:id',
    component: EditReferrerComponent
  },
  { path: 'indexReferrer',
    component: IndexReferrerComponent
  }
];
export const TheReferenceRoutes: Routes = [
  { path: 'createTheReference',
    component: CreateTheReferenceComponent
  },
  {
    path: 'editTheReference/:id',
    component: EditTheReferenceComponent
  },
  { path: 'indexTheReference',
    component: IndexTheReferenceComponent
  }
];
export const ReferrerGroupRoutes: Routes = [
  { path: 'createReferrerGroup',
    component: CreateReferrerGroupComponent
  },
  {
    path: 'editReferrerGroup/:id',
    component: EditReferrerGroupComponent
  },
  { path: 'indexReferrerGroup',
    component: IndexReferrerGroupComponent
  }
];
export const ReferenceEngineRoutes: Routes = [
  { path: 'createReferenceEngine',
    component: CreateReferenceEngineComponent
  },
  {
    path: 'editReferenceEngine/:id',
    component: EditReferenceEngineComponent
  },
  { path: 'indexReferenceEngine',
    component: IndexReferenceEngineComponent
  }
];
export const QuestionRoutes: Routes = [
  { path: 'createQuestion',
    component: CreateQuestionComponent
  },
  {
    path: 'editQuestion/:id',
    component: EditQuestionComponent
  },
  { path: 'indexQuestion',
    component: IndexQuestionComponent
  }
];
export const TheResponseRoutes: Routes = [
  { path: 'createTheResponse',
    component: CreateTheResponseComponent
  },
  {
    path: 'editTheResponse/:id',
    component: EditTheResponseComponent
  },
  { path: 'indexTheResponse',
    component: IndexTheResponseComponent
  }
];
export const QuestionGroupRoutes: Routes = [
  { path: 'createQuestionGroup',
    component: CreateQuestionGroupComponent
  },
  {
    path: 'editQuestionGroup/:id',
    component: EditQuestionGroupComponent
  },
  { path: 'indexQuestionGroup',
    component: IndexQuestionGroupComponent
  }
];
export const AdminRoutes: Routes = [
  { path: 'createAdmin',
    component: CreateAdminComponent
  },
  {
    path: 'editAdmin/:id',
    component: EditAdminComponent
  },
  { path: 'indexAdmin',
    component: IndexAdminComponent
  }
];
export const ActivityRoutes: Routes = [
  { path: 'createActivity',
    component: CreateActivityComponent
  },
  {
    path: 'editActivity/:id',
    component: EditActivityComponent
  },
  { path: 'indexActivity',
    component: IndexActivityComponent
  }
];
export const CommentRoutes: Routes = [
  { path: 'createComment',
    component: CreateCommentComponent
  },
  {
    path: 'editComment/:id',
    component: EditCommentComponent
  },
  { path: 'indexComment',
    component: IndexCommentComponent
  }
];
export const AnswerRoutes: Routes = [
  { path: 'createAnswer',
    component: CreateAnswerComponent
  },
  {
    path: 'editAnswer/:id',
    component: EditAnswerComponent
  },
  { path: 'indexAnswer',
    component: IndexAnswerComponent
  }
];
export const ReferenceGroupLinkRoutes: Routes = [
  { path: 'createReferenceGroupLink',
    component: CreateReferenceGroupLinkComponent
  },
  {
    path: 'editReferenceGroupLink/:id',
    component: EditReferenceGroupLinkComponent
  },
  { path: 'indexReferenceGroupLink',
    component: IndexReferenceGroupLinkComponent
  }
];
