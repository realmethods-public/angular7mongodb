import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule, MatFormFieldModule, MatSelectModule } from '@angular/material';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MatSidenavModule } from '@angular/material/sidenav'


import { IndexUserComponent } from './components/User/index/index.component';
import { CreateUserComponent } from './components/User/create/create.component';
import { EditUserComponent } from './components/User/edit/edit.component';
import { IndexReferrerComponent } from './components/Referrer/index/index.component';
import { CreateReferrerComponent } from './components/Referrer/create/create.component';
import { EditReferrerComponent } from './components/Referrer/edit/edit.component';
import { IndexTheReferenceComponent } from './components/TheReference/index/index.component';
import { CreateTheReferenceComponent } from './components/TheReference/create/create.component';
import { EditTheReferenceComponent } from './components/TheReference/edit/edit.component';
import { IndexReferrerGroupComponent } from './components/ReferrerGroup/index/index.component';
import { CreateReferrerGroupComponent } from './components/ReferrerGroup/create/create.component';
import { EditReferrerGroupComponent } from './components/ReferrerGroup/edit/edit.component';
import { IndexReferenceEngineComponent } from './components/ReferenceEngine/index/index.component';
import { CreateReferenceEngineComponent } from './components/ReferenceEngine/create/create.component';
import { EditReferenceEngineComponent } from './components/ReferenceEngine/edit/edit.component';
import { IndexQuestionComponent } from './components/Question/index/index.component';
import { CreateQuestionComponent } from './components/Question/create/create.component';
import { EditQuestionComponent } from './components/Question/edit/edit.component';
import { IndexTheResponseComponent } from './components/TheResponse/index/index.component';
import { CreateTheResponseComponent } from './components/TheResponse/create/create.component';
import { EditTheResponseComponent } from './components/TheResponse/edit/edit.component';
import { IndexQuestionGroupComponent } from './components/QuestionGroup/index/index.component';
import { CreateQuestionGroupComponent } from './components/QuestionGroup/create/create.component';
import { EditQuestionGroupComponent } from './components/QuestionGroup/edit/edit.component';
import { IndexAdminComponent } from './components/Admin/index/index.component';
import { CreateAdminComponent } from './components/Admin/create/create.component';
import { EditAdminComponent } from './components/Admin/edit/edit.component';
import { IndexActivityComponent } from './components/Activity/index/index.component';
import { CreateActivityComponent } from './components/Activity/create/create.component';
import { EditActivityComponent } from './components/Activity/edit/edit.component';
import { IndexCommentComponent } from './components/Comment/index/index.component';
import { CreateCommentComponent } from './components/Comment/create/create.component';
import { EditCommentComponent } from './components/Comment/edit/edit.component';
import { IndexAnswerComponent } from './components/Answer/index/index.component';
import { CreateAnswerComponent } from './components/Answer/create/create.component';
import { EditAnswerComponent } from './components/Answer/edit/edit.component';
import { IndexReferenceGroupLinkComponent } from './components/ReferenceGroupLink/index/index.component';
import { CreateReferenceGroupLinkComponent } from './components/ReferenceGroupLink/create/create.component';
import { EditReferenceGroupLinkComponent } from './components/ReferenceGroupLink/edit/edit.component';

import * as appRoutes from './routerConfig';

import { UserService } from './services/User.service';
import { ReferrerService } from './services/Referrer.service';
import { TheReferenceService } from './services/TheReference.service';
import { ReferrerGroupService } from './services/ReferrerGroup.service';
import { ReferenceEngineService } from './services/ReferenceEngine.service';
import { QuestionService } from './services/Question.service';
import { TheResponseService } from './services/TheResponse.service';
import { QuestionGroupService } from './services/QuestionGroup.service';
import { AdminService } from './services/Admin.service';
import { ActivityService } from './services/Activity.service';
import { CommentService } from './services/Comment.service';
import { AnswerService } from './services/Answer.service';
import { ReferenceGroupLinkService } from './services/ReferenceGroupLink.service';

@NgModule({
  declarations: [
    IndexUserComponent,
    CreateUserComponent,
    EditUserComponent,
    IndexReferrerComponent,
    CreateReferrerComponent,
    EditReferrerComponent,
    IndexTheReferenceComponent,
    CreateTheReferenceComponent,
    EditTheReferenceComponent,
    IndexReferrerGroupComponent,
    CreateReferrerGroupComponent,
    EditReferrerGroupComponent,
    IndexReferenceEngineComponent,
    CreateReferenceEngineComponent,
    EditReferenceEngineComponent,
    IndexQuestionComponent,
    CreateQuestionComponent,
    EditQuestionComponent,
    IndexTheResponseComponent,
    CreateTheResponseComponent,
    EditTheResponseComponent,
    IndexQuestionGroupComponent,
    CreateQuestionGroupComponent,
    EditQuestionGroupComponent,
    IndexAdminComponent,
    CreateAdminComponent,
    EditAdminComponent,
    IndexActivityComponent,
    CreateActivityComponent,
    EditActivityComponent,
    IndexCommentComponent,
    CreateCommentComponent,
    EditCommentComponent,
    IndexAnswerComponent,
    CreateAnswerComponent,
    EditAnswerComponent,
    IndexReferenceGroupLinkComponent,
    CreateReferenceGroupLinkComponent,
    EditReferenceGroupLinkComponent,
    AppComponent
  ],
  imports: [

    BrowserModule, 
    NgbModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
	MatMomentDateModule,
    BrowserAnimationsModule,
	HttpClientModule, 
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,    
    RouterModule.forRoot(appRoutes.UserRoutes), 
    RouterModule.forRoot(appRoutes.ReferrerRoutes), 
    RouterModule.forRoot(appRoutes.TheReferenceRoutes), 
    RouterModule.forRoot(appRoutes.ReferrerGroupRoutes), 
    RouterModule.forRoot(appRoutes.ReferenceEngineRoutes), 
    RouterModule.forRoot(appRoutes.QuestionRoutes), 
    RouterModule.forRoot(appRoutes.TheResponseRoutes), 
    RouterModule.forRoot(appRoutes.QuestionGroupRoutes), 
    RouterModule.forRoot(appRoutes.AdminRoutes), 
    RouterModule.forRoot(appRoutes.ActivityRoutes), 
    RouterModule.forRoot(appRoutes.CommentRoutes), 
    RouterModule.forRoot(appRoutes.AnswerRoutes), 
    RouterModule.forRoot(appRoutes.ReferenceGroupLinkRoutes), 
  ],
  providers: [UserService,ReferrerService,TheReferenceService,ReferrerGroupService,ReferenceEngineService,QuestionService,TheResponseService,QuestionGroupService,AdminService,ActivityService,CommentService,AnswerService,ReferenceGroupLinkService],
  bootstrap: [AppComponent]
})
export class AppModule { }
