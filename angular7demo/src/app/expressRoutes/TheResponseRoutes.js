// theResponseRoutes.js

var express = require('express');
var app = express();
var theResponseRoutes = express.Router();

// Require Item model in our routes module
var TheResponse = require('../models/TheResponse');

// Defined store route
theResponseRoutes.route('/add').post(function (req, res) {
	var theResponse = new TheResponse(req.body);
	theResponse.save()
    .then(item => {
    	res.status(200).json({'theResponse': 'TheResponse added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
theResponseRoutes.route('/').get(function (req, res) {
	TheResponse.find(function (err, theResponses){
		if(err){
			console.log(err);
		}
		else {
			res.json(theResponses);
		}
	});
});

// Defined edit route
theResponseRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	TheResponse.findById(id, function (err, theResponse){
		res.json(theResponse);
	});
});

//  Defined update route
theResponseRoutes.route('/update/:id').post(function (req, res) {
	TheResponse.findById(req.params.id, function(err, theResponse) {
		if (!theResponse)
			return next(new Error('Could not load a TheResponse Document using id ' + req.params.id));
		else {
            theResponse.responseText = req.body.responseText;
            theResponse.gotoQuestionId = req.body.gotoQuestionId;

			theResponse.save().then(theResponse => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
theResponseRoutes.route('/delete/:id').get(function (req, res) {
   TheResponse.findOneAndDelete({_id: req.params.id}, function(err, theResponse){
        if(err) res.json(err);
        else res.json('Successfully removed ' + TheResponse + ' using id ' + req.params.id );
    });
});

module.exports = theResponseRoutes;