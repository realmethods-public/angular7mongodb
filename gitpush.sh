#!/bin/bash

echo reset
git reset

git config --global user.email "dev@realmethods.com"
git config --global user.name "Scrum Master"
 
echo init the repository
git init

echo add all files from root dir below, with ignore dirs and files in the .gitignore
git add .

echo 'commit all the files'
git commit -m "initial commit"

echo 'remove the repository'
git remote rm Angular7MongoDB

echo 'add a remote pseudo for the Angular7MongoDB repository'
git remote add Angular7MongoDB https://complexmathguy:6969Cutlass!!@gitlab.com/complexmathguy/Angular7MongoDB

echo 'delete tag'
git tag -d latest

git push --delete Angular7MongoDB latest

echo 'add tag'
git tag latest

echo 'push tag'
git push Angular7MongoDB latest

echo 'push the commits to the repository and append a new branch Angular7MongoDB to Angular7MongoDB'
git push Angular7MongoDB master:Angular7MongoDB
