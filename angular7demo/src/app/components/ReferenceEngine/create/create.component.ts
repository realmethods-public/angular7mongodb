import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ReferenceEngineService } from '../../../services/ReferenceEngine.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../ReferenceEngine/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateReferenceEngineComponent extends SubBaseComponent implements OnInit {

  title = 'Add ReferenceEngine';
  referenceEngineForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private referenceEngineservice: ReferenceEngineService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.referenceEngineForm = this.fb.group({
      name: ['', Validators.required],
      active: ['', Validators.required],
      MainQuestionGroup: ['', ],
      Purpose: ['', ]
   });
  }
  addReferenceEngine(name, active, MainQuestionGroup, Purpose) {
      this.referenceEngineservice.addReferenceEngine(name, active, MainQuestionGroup, Purpose)
      	.then(success => this.router.navigate(['/indexReferenceEngine']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
