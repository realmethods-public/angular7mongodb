import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { TheReferenceService } from './TheReference.service';

describe('TheReferenceService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [TheReferenceService] });
	});

  it('should be created', () => {
    const service: TheReferenceService = TestBed.get(TheReferenceService);
    expect(service).toBeTruthy();
  });
});
