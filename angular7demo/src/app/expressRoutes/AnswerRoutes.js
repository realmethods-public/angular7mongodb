// answerRoutes.js

var express = require('express');
var app = express();
var answerRoutes = express.Router();

// Require Item model in our routes module
var Answer = require('../models/Answer');

// Defined store route
answerRoutes.route('/add').post(function (req, res) {
	var answer = new Answer(req.body);
	answer.save()
    .then(item => {
    	res.status(200).json({'answer': 'Answer added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
answerRoutes.route('/').get(function (req, res) {
	Answer.find(function (err, answers){
		if(err){
			console.log(err);
		}
		else {
			res.json(answers);
		}
	});
});

// Defined edit route
answerRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Answer.findById(id, function (err, answer){
		res.json(answer);
	});
});

//  Defined update route
answerRoutes.route('/update/:id').post(function (req, res) {
	Answer.findById(req.params.id, function(err, answer) {
		if (!answer)
			return next(new Error('Could not load a Answer Document using id ' + req.params.id));
		else {
            answer.Question = req.body.Question;
            answer.Response = req.body.Response;

			answer.save().then(answer => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
answerRoutes.route('/delete/:id').get(function (req, res) {
   Answer.findOneAndDelete({_id: req.params.id}, function(err, answer){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Answer + ' using id ' + req.params.id );
    });
});

module.exports = answerRoutes;