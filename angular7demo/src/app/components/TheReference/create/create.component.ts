import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TheReferenceService } from '../../../services/TheReference.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../TheReference/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateTheReferenceComponent extends SubBaseComponent implements OnInit {

  title = 'Add TheReference';
  theReferenceForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private theReferenceservice: TheReferenceService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.theReferenceForm = this.fb.group({
      dateLastUpdated: ['', Validators.required],
      active: ['', Validators.required],
      dateTimeLastViewedExternally: ['', Validators.required],
      makeViewableToUser: ['', Validators.required],
      dateLastSent: ['', Validators.required],
      numDaysToExpire: ['', Validators.required],
      QuestionGroup: ['', ],
      Status: ['', ],
      Type: ['', ],
      User: ['', ],
      Referrer: ['', ],
      Answers: ['', ],
      LastQuestionAnswered: ['', ]
   });
  }
  addTheReference(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered) {
      this.theReferenceservice.addTheReference(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered)
      	.then(success => this.router.navigate(['/indexTheReference']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
