var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Answer
var Answer = new Schema({
  Question: {
	type : Schema.Types.ObjectId
  },
  Response: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'answers'
});

module.exports = mongoose.model('Answer', Answer);