import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ReferrerService } from '../../../services/Referrer.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Referrer/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateReferrerComponent extends SubBaseComponent implements OnInit {

  title = 'Add Referrer';
  referrerForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private referrerservice: ReferrerService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.referrerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      emailAddress: ['', Validators.required],
      active: ['', Validators.required],
      Comments: ['', ]
   });
  }
  addReferrer(firstName, lastName, emailAddress, active, Comments) {
      this.referrerservice.addReferrer(firstName, lastName, emailAddress, active, Comments)
      	.then(success => this.router.navigate(['/indexReferrer']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
