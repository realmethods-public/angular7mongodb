var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for ReferenceEngine
var ReferenceEngine = new Schema({
  name: {
	type : String
  },
  active: {
	type : Boolean
  },
  MainQuestionGroup: {
	type : Schema.Types.ObjectId
  },
  Purpose: {
 	type : String
  },
},{
    collection: 'referenceEngines'
});

module.exports = mongoose.model('ReferenceEngine', ReferenceEngine);