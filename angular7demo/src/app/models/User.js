var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for User
var User = new Schema({
  password: {
	type : String
  },
  resumeLinkUrl: {
	type : String
  },
  linkedInUrl: {
	type : String
  },
  ReferenceProviders: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Referrer' }]
  },
  RefererGroups: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'ReferrerGroup' }]
  },
  ReferenceReceivers: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'Referrer' }]
  },
  ReferenceGroupLinks: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'ReferenceGroupLink' }]
  },
},{
    collection: 'users'
});

module.exports = mongoose.model('User', User);