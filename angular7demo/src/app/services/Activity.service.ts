import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Activity} from '../models/Activity';
import {UserService} from '../services/User.service';


@Injectable()
export class ActivityService {

	//********************************************************************
	// general holder 
	//********************************************************************
	activity : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {}
 	
	//********************************************************************
	// add a Activity 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addActivity(refObjId, createDateTime, Type, User) : Promise<any> {
    	const uri = 'http://localhost:4200/Activity/add';
    	const obj = {
      		refObjId: refObjId,
      		createDateTime: createDateTime,
      		Type: Type,
			User: User != null && User.length > 0 ? User : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Activity 
	// returns the results untouched as JSON representation of an
	// array of Activity models
	// delegates via URI to an ORM handler
	//********************************************************************
	getActivitys() {
    	const uri = 'http://localhost:4200/Activity';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Activity 
	// returns the results untouched as a JSON representation of a
	// Activity model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editActivity(id) {
    	const uri = 'http://localhost:4200/Activity/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Activity 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateActivity(refObjId, createDateTime, Type, User, id)  : Promise<any>  {
    	const uri = 'http://localhost:4200/Activity/update/' + id;
    	const obj = {
      		refObjId: refObjId,
      		createDateTime: createDateTime,
      		Type: Type,
			User: User != null && User.length > 0 ? User : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Activity 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteActivity(id)  : Promise<any> {
    	const uri = 'http://localhost:4000/Activity/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a User on a Activity
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignUser( activityId, userId ): Promise<any> {

		// get the Activity from storage
		this.loadHelper( activityId );
		
		// get the User from storage
		var user 	= new UserService(this.http).editUser(userId);
		
		// assign the User		
		this.activity.user = user;
      		
		// save the Activity
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a User on a Activity
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignUser( activityId ): Promise<any> {

		// get the Activity from storage
        this.loadHelper( activityId );
		
		// assign User to null		
		this.activity.user = null;
      		
		// save the Activity
		return this.saveHelper();
	}
	


	//********************************************************************
	// saveHelper - internal helper to save a Activity
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = 'http://localhost:4200/Activity/update/' + this.activity._id;		
		
    	return this
      			.http
      			.post(uri, this.activity)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Activity
	//********************************************************************	
	loadHelper( id ) {
		this.editActivity(id)
        		.subscribe(res => {
        			this.activity = res;
      			});
	}
}