import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionGroupService } from '../../../services/QuestionGroup.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../QuestionGroup/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditQuestionGroupComponent extends SubBaseComponent implements OnInit {

  questionGroup: any;
  questionGroupForm: FormGroup;
  title = 'Edit QuestionGroup';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: QuestionGroupService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.questionGroupForm = this.fb.group({
      aggregateScore: ['', Validators.required],
      weight: ['', Validators.required],
      name: ['', Validators.required],
      Questions: ['', ],
      QuestionGroups: ['', ]
   });
  }
  updateQuestionGroup(aggregateScore, weight, name, Questions, QuestionGroups) {
    this.route.params.subscribe(params => {
    	this.service.updateQuestionGroup(aggregateScore, weight, name, Questions, QuestionGroups, params['id'])
      		.then(success => this.router.navigate(['/indexQuestionGroup']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.questionGroup = this.service.editQuestionGroup(params['id']).subscribe(res => {
        this.questionGroup = res;
      });
    });
    
    super.ngOnInit();
  }
}
