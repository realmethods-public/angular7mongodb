import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReferrerGroupService } from '../../../services/ReferrerGroup.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../ReferrerGroup/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditReferrerGroupComponent extends SubBaseComponent implements OnInit {

  referrerGroup: any;
  referrerGroupForm: FormGroup;
  title = 'Edit ReferrerGroup';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferrerGroupService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.referrerGroupForm = this.fb.group({
      name: ['', Validators.required],
      dateTimeLastViewedExternally: ['', Validators.required],
      References: ['', ]
   });
  }
  updateReferrerGroup(name, dateTimeLastViewedExternally, References) {
    this.route.params.subscribe(params => {
    	this.service.updateReferrerGroup(name, dateTimeLastViewedExternally, References, params['id'])
      		.then(success => this.router.navigate(['/indexReferrerGroup']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.referrerGroup = this.service.editReferrerGroup(params['id']).subscribe(res => {
        this.referrerGroup = res;
      });
    });
    
    super.ngOnInit();
  }
}
