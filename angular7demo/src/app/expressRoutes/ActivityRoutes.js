// activityRoutes.js

var express = require('express');
var app = express();
var activityRoutes = express.Router();

// Require Item model in our routes module
var Activity = require('../models/Activity');

// Defined store route
activityRoutes.route('/add').post(function (req, res) {
	var activity = new Activity(req.body);
	activity.save()
    .then(item => {
    	res.status(200).json({'activity': 'Activity added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
activityRoutes.route('/').get(function (req, res) {
	Activity.find(function (err, activitys){
		if(err){
			console.log(err);
		}
		else {
			res.json(activitys);
		}
	});
});

// Defined edit route
activityRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Activity.findById(id, function (err, activity){
		res.json(activity);
	});
});

//  Defined update route
activityRoutes.route('/update/:id').post(function (req, res) {
	Activity.findById(req.params.id, function(err, activity) {
		if (!activity)
			return next(new Error('Could not load a Activity Document using id ' + req.params.id));
		else {
            activity.refObjId = req.body.refObjId;
            activity.createDateTime = req.body.createDateTime;
            activity.Type = req.body.Type;
            activity.User = req.body.User;

			activity.save().then(activity => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
activityRoutes.route('/delete/:id').get(function (req, res) {
   Activity.findOneAndDelete({_id: req.params.id}, function(err, activity){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Activity + ' using id ' + req.params.id );
    });
});

module.exports = activityRoutes;