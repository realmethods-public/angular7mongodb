var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Question
var Question = new Schema({
  weight: {
	type : String
  },
  questionText: {
	type : String
  },
  identifier: {
	type : String
  },
  mustBeAnswered: {
	type : Boolean
  },
  responseExclusive: {
	type : Boolean
  },
  Responses: {
 	type : [{ type: Schema.Types.ObjectId, ref: 'TheResponse' }]
  },
},{
    collection: 'questions'
});

module.exports = mongoose.model('Question', Question);