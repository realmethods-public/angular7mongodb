// theReferenceRoutes.js

var express = require('express');
var app = express();
var theReferenceRoutes = express.Router();

// Require Item model in our routes module
var TheReference = require('../models/TheReference');

// Defined store route
theReferenceRoutes.route('/add').post(function (req, res) {
	var theReference = new TheReference(req.body);
	theReference.save()
    .then(item => {
    	res.status(200).json({'theReference': 'TheReference added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
theReferenceRoutes.route('/').get(function (req, res) {
	TheReference.find(function (err, theReferences){
		if(err){
			console.log(err);
		}
		else {
			res.json(theReferences);
		}
	});
});

// Defined edit route
theReferenceRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	TheReference.findById(id, function (err, theReference){
		res.json(theReference);
	});
});

//  Defined update route
theReferenceRoutes.route('/update/:id').post(function (req, res) {
	TheReference.findById(req.params.id, function(err, theReference) {
		if (!theReference)
			return next(new Error('Could not load a TheReference Document using id ' + req.params.id));
		else {
            theReference.dateLastUpdated = req.body.dateLastUpdated;
            theReference.active = req.body.active;
            theReference.dateTimeLastViewedExternally = req.body.dateTimeLastViewedExternally;
            theReference.makeViewableToUser = req.body.makeViewableToUser;
            theReference.dateLastSent = req.body.dateLastSent;
            theReference.numDaysToExpire = req.body.numDaysToExpire;
            theReference.QuestionGroup = req.body.QuestionGroup;
            theReference.Status = req.body.Status;
            theReference.Type = req.body.Type;
            theReference.User = req.body.User;
            theReference.Referrer = req.body.Referrer;
            theReference.Answers = req.body.Answers;
            theReference.LastQuestionAnswered = req.body.LastQuestionAnswered;

			theReference.save().then(theReference => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
theReferenceRoutes.route('/delete/:id').get(function (req, res) {
   TheReference.findOneAndDelete({_id: req.params.id}, function(err, theReference){
        if(err) res.json(err);
        else res.json('Successfully removed ' + TheReference + ' using id ' + req.params.id );
    });
});

module.exports = theReferenceRoutes;