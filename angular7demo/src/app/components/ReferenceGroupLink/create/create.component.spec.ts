import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CreateReferenceGroupLinkComponent } from './create.component';

describe('CreateReferenceGroupLinkComponent', () => {
  let component: CreateReferenceGroupLinkComponent;
  let fixture: ComponentFixture<CreateReferenceGroupLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ CreateReferenceGroupLinkComponent ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReferenceGroupLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
