import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {TheResponse} from '../models/TheResponse';


@Injectable()
export class TheResponseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	theResponse : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {}
 	
	//********************************************************************
	// add a TheResponse 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addTheResponse(responseText, gotoQuestionId) : Promise<any> {
    	const uri = 'http://localhost:4200/TheResponse/add';
    	const obj = {
      		responseText: responseText,
			gotoQuestionId: gotoQuestionId
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all TheResponse 
	// returns the results untouched as JSON representation of an
	// array of TheResponse models
	// delegates via URI to an ORM handler
	//********************************************************************
	getTheResponses() {
    	const uri = 'http://localhost:4200/TheResponse';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a TheResponse 
	// returns the results untouched as a JSON representation of a
	// TheResponse model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editTheResponse(id) {
    	const uri = 'http://localhost:4200/TheResponse/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a TheResponse 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateTheResponse(responseText, gotoQuestionId, id)  : Promise<any>  {
    	const uri = 'http://localhost:4200/TheResponse/update/' + id;
    	const obj = {
      		responseText: responseText,
			gotoQuestionId: gotoQuestionId
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a TheResponse 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteTheResponse(id)  : Promise<any> {
    	const uri = 'http://localhost:4000/TheResponse/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	

	//********************************************************************
	// saveHelper - internal helper to save a TheResponse
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = 'http://localhost:4200/TheResponse/update/' + this.theResponse._id;		
		
    	return this
      			.http
      			.post(uri, this.theResponse)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a TheResponse
	//********************************************************************	
	loadHelper( id ) {
		this.editTheResponse(id)
        		.subscribe(res => {
        			this.theResponse = res;
      			});
	}
}