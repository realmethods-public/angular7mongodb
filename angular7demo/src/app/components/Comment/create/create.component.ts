import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CommentService } from '../../../services/Comment.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Comment/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateCommentComponent extends SubBaseComponent implements OnInit {

  title = 'Add Comment';
  commentForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private commentservice: CommentService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.commentForm = this.fb.group({
      commentText: ['', Validators.required],
      Source: ['', ]
   });
  }
  addComment(commentText, Source) {
      this.commentservice.addComment(commentText, Source)
      	.then(success => this.router.navigate(['/indexComment']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
