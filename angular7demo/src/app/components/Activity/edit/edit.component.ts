import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityService } from '../../../services/Activity.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Activity/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditActivityComponent extends SubBaseComponent implements OnInit {

  activity: any;
  activityForm: FormGroup;
  title = 'Edit Activity';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ActivityService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.activityForm = this.fb.group({
      refObjId: ['', Validators.required],
      createDateTime: ['', Validators.required],
      Type: ['', Validators.required],
      User: ['', ]
   });
  }
  updateActivity(refObjId, createDateTime, Type, User) {
    this.route.params.subscribe(params => {
    	this.service.updateActivity(refObjId, createDateTime, Type, User, params['id'])
      		.then(success => this.router.navigate(['/indexActivity']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.activity = this.service.editActivity(params['id']).subscribe(res => {
        this.activity = res;
      });
    });
    
    super.ngOnInit();
  }
}
