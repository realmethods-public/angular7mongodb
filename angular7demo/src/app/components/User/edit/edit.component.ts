import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/User.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../User/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditUserComponent extends SubBaseComponent implements OnInit {

  user: any;
  userForm: FormGroup;
  title = 'Edit User';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: UserService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.userForm = this.fb.group({
      password: ['', Validators.required],
      resumeLinkUrl: ['', Validators.required],
      linkedInUrl: ['', Validators.required],
      ReferenceProviders: ['', ],
      RefererGroups: ['', ],
      ReferenceReceivers: ['', ],
      ReferenceGroupLinks: ['', ]
   });
  }
  updateUser(firstName, lastName, emailAddress, active, Comments, password, resumeLinkUrl, linkedInUrl, ReferenceProviders, RefererGroups, ReferenceReceivers, ReferenceGroupLinks) {
    this.route.params.subscribe(params => {
    	this.service.updateUser(firstName, lastName, emailAddress, active, Comments, password, resumeLinkUrl, linkedInUrl, ReferenceProviders, RefererGroups, ReferenceReceivers, ReferenceGroupLinks, params['id'])
      		.then(success => this.router.navigate(['/indexUser']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.user = this.service.editUser(params['id']).subscribe(res => {
        this.user = res;
      });
    });
    
    super.ngOnInit();
  }
}
