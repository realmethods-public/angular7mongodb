import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ReferrerGroupService } from './ReferrerGroup.service';

describe('ReferrerGroupService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [ReferrerGroupService] });
	});

  it('should be created', () => {
    const service: ReferrerGroupService = TestBed.get(ReferrerGroupService);
    expect(service).toBeTruthy();
  });
});
