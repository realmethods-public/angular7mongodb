import { TheReferenceService } from '../../../services/TheReference.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TheReference } from '../../../models/TheReference';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexTheReferenceComponent implements OnInit {

  theReferences: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: TheReferenceService) {}

  ngOnInit() {
    this.getTheReferences();
  }

  getTheReferences() {
    this.service.getTheReferences().subscribe(res => {
      this.theReferences = res;
    });
  }

  deleteTheReference(id) {
    this.service.deleteTheReference(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexTheReference']));
			});  }
}
