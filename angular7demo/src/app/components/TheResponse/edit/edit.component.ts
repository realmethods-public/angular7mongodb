import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TheResponseService } from '../../../services/TheResponse.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../TheResponse/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditTheResponseComponent extends SubBaseComponent implements OnInit {

  theResponse: any;
  theResponseForm: FormGroup;
  title = 'Edit TheResponse';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: TheResponseService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.theResponseForm = this.fb.group({
      responseText: ['', Validators.required],
      gotoQuestionId: ['', Validators.required]
   });
  }
  updateTheResponse(responseText, gotoQuestionId) {
    this.route.params.subscribe(params => {
    	this.service.updateTheResponse(responseText, gotoQuestionId, params['id'])
      		.then(success => this.router.navigate(['/indexTheResponse']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.theResponse = this.service.editTheResponse(params['id']).subscribe(res => {
        this.theResponse = res;
      });
    });
    
    super.ngOnInit();
  }
}
