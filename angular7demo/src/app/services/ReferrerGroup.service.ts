import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {ReferrerGroup} from '../models/ReferrerGroup';
import {TheReferenceService} from '../services/TheReference.service';


@Injectable()
export class ReferrerGroupService {

	//********************************************************************
	// general holder 
	//********************************************************************
	referrerGroup : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {}
 	
	//********************************************************************
	// add a ReferrerGroup 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addReferrerGroup(name, dateTimeLastViewedExternally, References) : Promise<any> {
    	const uri = 'http://localhost:4200/ReferrerGroup/add';
    	const obj = {
      		name: name,
      		dateTimeLastViewedExternally: dateTimeLastViewedExternally,
			References: References != null && References.length > 0 ? References : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all ReferrerGroup 
	// returns the results untouched as JSON representation of an
	// array of ReferrerGroup models
	// delegates via URI to an ORM handler
	//********************************************************************
	getReferrerGroups() {
    	const uri = 'http://localhost:4200/ReferrerGroup';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a ReferrerGroup 
	// returns the results untouched as a JSON representation of a
	// ReferrerGroup model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editReferrerGroup(id) {
    	const uri = 'http://localhost:4200/ReferrerGroup/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a ReferrerGroup 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateReferrerGroup(name, dateTimeLastViewedExternally, References, id)  : Promise<any>  {
    	const uri = 'http://localhost:4200/ReferrerGroup/update/' + id;
    	const obj = {
      		name: name,
      		dateTimeLastViewedExternally: dateTimeLastViewedExternally,
			References: References != null && References.length > 0 ? References : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a ReferrerGroup 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteReferrerGroup(id)  : Promise<any> {
    	const uri = 'http://localhost:4000/ReferrerGroup/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	
	//********************************************************************
	// adds one or more referencesIds as a References 
	// to a ReferrerGroup
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addReferences( referrerGroupId, referencesIds ): Promise<any> {

		// get the ReferrerGroup
		this.loadHelper( referrerGroupId );
				
		// split on a comma with no spaces
		var idList = referencesIds.split(',')

		// iterate over array of references ids
		idList.forEach(function (id) {
			// read the TheReference		
			var theReference = new TheReferenceService(this.http).editTheReference(id);	
			// add the TheReference if not already assigned
			if ( this.referrerGroup.references.indexOf(theReference) == -1 )
				this.referrerGroup.references.push(theReference);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more referencesIds as a References 
	// from a ReferrerGroup
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeReferences( referrerGroupId, referencesIds ): Promise<any> {
		
		// get the ReferrerGroup
		this.loadHelper( referrerGroupId );

				
		// split on a comma with no spaces
		var idList 					= referencesIds.split(',');
		var references 	= this.referrerGroup.references;
		
		if ( references != null && referencesIds != null ) {
		
			// iterate over array of references ids
			references.forEach(function (obj) {				
				if ( referencesIds.indexOf(obj._id) > -1 ) {
					 // remove the TheReference
					this.referrerGroup.references.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a ReferrerGroup
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = 'http://localhost:4200/ReferrerGroup/update/' + this.referrerGroup._id;		
		
    	return this
      			.http
      			.post(uri, this.referrerGroup)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a ReferrerGroup
	//********************************************************************	
	loadHelper( id ) {
		this.editReferrerGroup(id)
        		.subscribe(res => {
        			this.referrerGroup = res;
      			});
	}
}