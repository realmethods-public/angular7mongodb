// commentRoutes.js

var express = require('express');
var app = express();
var commentRoutes = express.Router();

// Require Item model in our routes module
var Comment = require('../models/Comment');

// Defined store route
commentRoutes.route('/add').post(function (req, res) {
	var comment = new Comment(req.body);
	comment.save()
    .then(item => {
    	res.status(200).json({'comment': 'Comment added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
commentRoutes.route('/').get(function (req, res) {
	Comment.find(function (err, comments){
		if(err){
			console.log(err);
		}
		else {
			res.json(comments);
		}
	});
});

// Defined edit route
commentRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Comment.findById(id, function (err, comment){
		res.json(comment);
	});
});

//  Defined update route
commentRoutes.route('/update/:id').post(function (req, res) {
	Comment.findById(req.params.id, function(err, comment) {
		if (!comment)
			return next(new Error('Could not load a Comment Document using id ' + req.params.id));
		else {
            comment.commentText = req.body.commentText;
            comment.Source = req.body.Source;

			comment.save().then(comment => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
commentRoutes.route('/delete/:id').get(function (req, res) {
   Comment.findOneAndDelete({_id: req.params.id}, function(err, comment){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Comment + ' using id ' + req.params.id );
    });
});

module.exports = commentRoutes;