var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for TheResponse
var TheResponse = new Schema({
  responseText: {
	type : String
  },
  gotoQuestionId: {
	type : String
  },
},{
    collection: 'theResponses'
});

module.exports = mongoose.model('TheResponse', TheResponse);