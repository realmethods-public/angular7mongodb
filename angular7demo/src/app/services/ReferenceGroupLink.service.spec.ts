import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ReferenceGroupLinkService } from './ReferenceGroupLink.service';

describe('ReferenceGroupLinkService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [ReferenceGroupLinkService] });
	});

  it('should be created', () => {
    const service: ReferenceGroupLinkService = TestBed.get(ReferenceGroupLinkService);
    expect(service).toBeTruthy();
  });
});
